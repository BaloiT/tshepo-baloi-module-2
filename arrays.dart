void main() {
  List winningApp = [
    "FNB",
    "SnapScan",
    "Live Inspect",
    "WumDrop",
    "Domestly",
    "Shyft",
    "Khula Ecosystem",
    "Naked Insurance",
    "Easy Equities",
    "Ambani Africa"
  ];

  //Sort and print Array
  print(".....A.Print and Sort Array................");
  List appList = winningApp.toList();
  appList.sort();
  print("The winning App since 2012 is $appList");

//winning Apps for 2017 and 2018
  print("......B.Winning Apps for 2017 and 2018.............");
  print("The Winning App of 2017 is ${winningApp[5]}");
  print("The Winning App of 2018 is ${winningApp[6]}");

//Printing Number in Array
  print(".....C.Total Number of Apps........................");
  print("Total Number of Apps from Array is ${winningApp.length.toString()}");
}
