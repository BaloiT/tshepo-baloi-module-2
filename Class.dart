class AppWinner {
  String? name;
  String? category;
  String? developer;
  int? year;

  void upperCase() {
    print(name?.toUpperCase());
  }
}

void main() {
  var winner = new AppWinner();
  print(".................A. Print object of a class.........");
  winner.name = "Shyft";
  winner.category = "Digital Banking";
  winner.developer = "Standard Bank";
  winner.year = 2017;
//use object to print names
  print("Name of an App is ${winner.name}");
  print("Sector/Category of an App is ${winner.category}");
  print("The Developers Are ${winner.developer}");
  print("The year it won MTN Business App of the year ${winner.year}");

//transform App Name to capital letter
  print("..........B.transform App name to Capital letter....");
  print("Function inside a class to trasform App Name");
  winner.upperCase();
}
